package com.example.tpfinalandroid;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ProjectsList extends AppCompatActivity {

    FirebaseFirestore database = FirebaseFirestore.getInstance();
    private String email;
    private ArrayList<String> project = new ArrayList();
    private TextInputEditText todoInput;
    private Button addTodoList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.projects_list);
        todoInput = findViewById(R.id.todoContent);


        this.addTodoList = findViewById(R.id.add);

        this.getExtra();
        this.getTodoList();
        this.listeningFirebase();
    }

    public void onAddTodoList(View v){
        DocumentReference docRef = database.collection("todos").document(this.email);
        docRef.update("todo", FieldValue.arrayUnion(this.todoInput.getText().toString()));
        getTodoList();
    }

    private void getTodoList(){
        DocumentReference cRef = database.collection("todos").document(this.email);
        cRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    if(document.exists()) {
                        ProjectsList.this.createAllTodolistView((ArrayList<String>) document.getData().get("todo"));
                    }
                    else{
                        Log.d("ImportantINFO","No document" );
                    }
                }
                else {
                    Log.d("ImportantINFO", "No documents at all ");
                }
        }
    });
    }

    private void getExtra(){
        final Intent intent = getIntent();
        if( intent.hasExtra("email")){
            this.email = intent.getStringExtra("email");
            Log.d("GETEXTRA", this.email);
        }
        else{
            Log.d("GETEXTRA", "Error getting extra: ");
        }
    }

    private void createAllTodolistView(ArrayList<String> listProject) {
        this.project = listProject;

        ListView list = findViewById(R.id.listView);
        ArrayList<TextProject> listLayout = new ArrayList<>();

        for (String nom : this.project){
            TextProject newts = new TextProject(nom,this.email);
            listLayout.add(newts);
        }

        MyListAdapter adapter = new MyListAdapter(this,listLayout);
        list.setAdapter(adapter);
    }

    public void listeningFirebase() {
        final DocumentReference docRef = database.collection("todos")
                .document(this.email);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("TAG", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    getTodoList();
                } else {
                    Log.d("TAG", "Current data: null");
                }
            }
    });

}}
