package com.example.tpfinalandroid;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class MyListAdapter extends ArrayAdapter<TextProject> {
    FirebaseFirestore database = FirebaseFirestore.getInstance();
    String nom;
    String email;
    public MyListAdapter(@NonNull Context context, ArrayList<TextProject> list) {
        super(context,0,list);
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        TextProject ts = getItem(position);
        this.nom = ts.getTexte();
        this.email = ts.getMail();
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.projects, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.textView);
        textView.setText(ts.getTexte());
        Button supprimer = convertView.findViewById(R.id.supprimer);
        supprimer.setOnClickListener(v -> supprimer(ts.getTexte()));
        return  convertView;
    }

    public void supprimer(String text){
        DocumentReference docRef = database.collection("todos").document(this.email);
        docRef.update("todo", FieldValue.arrayRemove(text));
    }

}
