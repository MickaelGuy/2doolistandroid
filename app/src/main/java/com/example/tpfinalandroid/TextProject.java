package com.example.tpfinalandroid;

public class TextProject {
    private String texte;
    private String mail;

    TextProject(String text,String mail){
        this.mail = mail;
        this.texte = text;
    }

    public String getTexte() {
        return texte;
    }

    public String getMail(){ return  mail;}
}
